alias Cldr
alias Cldr.Territory

alias Invoicex.{Document, Ecommerce, Extra}
alias Invoicex.Document.{Address, Buyer, Contact, Group, Invoice, Item, Number, Seller}
alias Invoicex.Ecommerce.Address, as: EcommerceAddress
alias Invoicex.Ecommerce.Discount, as: EcommerceDiscount
alias Invoicex.Ecommerce.{Interval, Region, Set, Source, Trader}
alias Invoicex.Ecommerce.Tax, as: EcommerceTax

alias Invoicex.Enum.{
  CountryCode,
  Currency,
  EmailType,
  IntervalType,
  InvoiceType,
  NumberType,
  PhoneNumberType
}

alias Invoicex.Extra.{Discount, Email, Money, Note, PhoneNumber, Reason, Tax}
