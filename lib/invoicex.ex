defmodule Invoicex do
  @moduledoc """
  Main invoicex module.
  """

  alias Ecto.Changeset

  @doc """
  Fetches ecto schema prefix specified in `:invoicex` configuration.

  This is used in enums and model schemas to specfify their database schema name.
  """
  @spec schema_prefix :: nil | atom
  def schema_prefix, do: Application.get_env(:invoicex, :schema_prefix, nil)

  def validate_order(changeset, false), do: changeset

  def validate_order(changeset, true) do
    changeset
    |> Changeset.validate_required(:order)
    |> Changeset.validate_number(:order, greater_than: 0)
  end
end
