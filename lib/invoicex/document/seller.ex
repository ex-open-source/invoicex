defmodule Invoicex.Document.Seller do
  @moduledoc """
  `Invoicex.Document.Invoice` seller model.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Document.{Address, Contact}
  alias Invoicex.Extra.Email

  @required_fields [:vendor]
  @fields [:logo_url | @required_fields]

  embedded_schema do
    embeds_many(:notification_emails, Email, on_replace: :delete)
    embeds_one(:address, Address, on_replace: :update)
    embeds_one(:contact, Contact, on_replace: :update)
    field(:logo_url, :string)
    field(:vendor, :string)
  end

  @doc false
  def changeset(address \\ %__MODULE__{}, attrs \\ %{}) do
    address
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:address)
    |> Changeset.cast_embed(:contact)
    |> Changeset.cast_embed(:notification_emails)
    |> Changeset.validate_required(@required_fields)
  end
end
