defmodule Invoicex.Document.Attachment do
  @moduledoc """
  Stores detailed data about attachments in `Invoice`.
  """

  use Ecto.Schema

  alias Ecto.Changeset

  @required_fields [:url]
  @fields @required_fields ++ [:description, :file_name, :label, :url]

  embedded_schema do
    field(:description, :string)
    field(:file_name, :string)
    field(:label, :string)
    field(:url, :string)
  end

  @doc false
  def changeset(address \\ %__MODULE__{}, attrs \\ %{}) do
    address
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
