defmodule Invoicex.Document.Item do
  @moduledoc """
  Stores invoice item data.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Extra.{Discount, Tax}

  @required_fields [:name, :price, :quantity]
  @fields [:description, :order] ++ @required_fields

  embedded_schema do
    embeds_one(:discount, Discount, on_replace: :update)
    embeds_one(:tax, Tax, on_replace: :update)
    field(:description, :string)
    field(:name, :string)
    field(:order, :integer)
    field(:price, :decimal)
    field(:quantity, :decimal)
    field(:total, :decimal, virtual: true)
  end

  @doc false
  def changeset(item \\ %__MODULE__{}, attrs \\ %{}, in_group \\ false) do
    item
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:discount)
    |> Changeset.cast_embed(:tax)
    |> Changeset.validate_number(:quantity, greater_than: 0)
    |> Changeset.validate_required(@required_fields)
    |> Invoicex.validate_order(in_group)
  end
end
