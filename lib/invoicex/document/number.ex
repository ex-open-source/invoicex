defmodule Invoicex.Document.Number do
  @moduledoc """
  `Invoicex.Document.Invoice` number allows to assign a generated number of specified type to invoice.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.Interval
  alias Invoicex.Enum.NumberType

  @fields [:type]
  @required_fields @fields

  embedded_schema do
    embeds_one(:interval, Interval, on_replace: :update)
    field(:type, NumberType)
    field(:value, :integer)
  end

  @doc false
  def changeset(address \\ %__MODULE__{}, attrs \\ %{}) do
    address
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
    |> validate_interval()
  end

  defp validate_interval(changeset) do
    type = Changeset.get_field(changeset, :type)
    Changeset.cast_embed(changeset, :interval, required: type == :recurring)
  end
end
