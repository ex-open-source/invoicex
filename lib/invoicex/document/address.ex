defmodule Invoicex.Document.Address do
  @moduledoc """
  Stores address data presented on invoice. 
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Document.Contact
  alias Invoicex.Enum.CountryCode

  @required_fields [:country_code, :data]
  @fields @required_fields

  embedded_schema do
    embeds_one(:contact, Contact, on_replace: :update)
    field(:country_code, CountryCode)
    field(:data, {:array, :string})
  end

  @doc false
  def changeset(address \\ %__MODULE__{}, attrs \\ %{}) do
    address
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:contact)
    |> Changeset.validate_length(:data, min: 1)
    |> Changeset.validate_required(@required_fields)
  end
end
