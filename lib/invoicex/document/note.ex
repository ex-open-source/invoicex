defmodule Invoicex.Document.Note do
  @moduledoc """
  Optional data which could be add to `Invoicex.Document.Invoice`.
  """

  use Ecto.Schema

  alias Ecto.Changeset

  @required_fields [:at, :label, :text]
  @fields @required_fields

  embedded_schema do
    field(:at, :date)
    field(:label, :string)
    field(:text, :string)
  end

  @doc false
  def changeset(note \\ %__MODULE__{}, attrs \\ %{}) do
    note
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
