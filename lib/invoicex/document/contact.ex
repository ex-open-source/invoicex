defmodule Invoicex.Document.Contact do
  @moduledoc """
  Stores contact data presented on invoice. 
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Extra.{Email, ContactNumber}

  @required_fields [:names, :surname]
  @fields [:websites | @required_fields]

  embedded_schema do
    embeds_many(:emails, Email, on_replace: :delete)
    embeds_many(:numbers, ContactNumber, on_replace: :delete)
    field(:names, {:array, :string})
    field(:surname, :string)
    field(:websites, {:array, :string})
  end

  @doc false
  def changeset(contact \\ %__MODULE__{}, attrs \\ %{}) do
    contact
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:emails)
    |> Changeset.cast_embed(:numbers)
    |> Changeset.validate_length(:names, min: 1)
    |> Changeset.validate_required(@required_fields)
  end
end
