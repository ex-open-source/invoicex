defmodule Invoicex.Helper do
  @moduledoc false

  @doc false
  defmacro copy_fields_from(module, opts \\ []) do
    quote(bind_quoted: [module: module, opts: opts]) do
      for {name, type} <- Invoicex.Helper.fields_for(module, opts) do
        case type do
          {:embed, %{cardinality: :many, field: field, on_replace: on_replace, related: related}} ->
            embeds_many(field, related, on_replace: on_replace)

          {:embed, %{cardinality: :one, field: field, on_replace: on_replace, related: related}} ->
            embeds_one(field, related, on_replace: on_replace)

          type ->
            default = module |> struct() |> Map.fetch!(name)
            field(name, type, default: default)
        end
      end
    end
  end

  @doc false
  def fields_for(module, opts \\ []), do: module.__schema__(:load) |> do_fields_for(opts)

  defp do_fields_for(keyword, except: except), do: Keyword.drop(keyword, except)
  defp do_fields_for(keyword, only: only), do: Keyword.take(keyword, only)
  defp do_fields_for(keyword, _opts), do: keyword
end
