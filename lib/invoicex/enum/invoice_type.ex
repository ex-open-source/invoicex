defmodule Invoicex.Enum.InvoiceType do
  @moduledoc """
  Stores `Invoicex.Document.Invoice` types.
  """

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :invoice_type,
    values: [:corrective, :credit_notes, :estimated, :normal, :pro_forma, :recurring]
end
