defmodule Invoicex.Enum.CountryCode do
  @moduledoc """
  This enum stores country codes provided by `ex_cldr_territories` hex package.
  """

  country_codes = Cldr.Territory.country_codes()

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :country_code,
    values: Enum.sort(country_codes)
end
