defmodule Invoicex.Enum.ContactNumberType do
  @moduledoc """
  Stores supported contact number types.
  """

  values = [
    :fixed_line,
    :fixed_line_or_mobile,
    :mobile,
    :pager,
    :personal_number,
    :premium_rate,
    :shared_cost,
    :toll_free,
    :uan,
    :voicemail,
    :voip
  ]

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :contact_number_type,
    values: values
end
