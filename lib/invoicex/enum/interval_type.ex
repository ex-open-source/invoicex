defmodule Invoicex.Enum.IntervalType do
  @moduledoc """
  Stores supported types for `Invoicex.Ecommerce.Interval`.
  """

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :number_interval,
    values: [:anuual, :biweekly, :daily, :monthly, :quartely, :weekly]
end
