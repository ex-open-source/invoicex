defmodule Invoicex.Enum.NumberType do
  @moduledoc """
  Stores `Invoicex.Document.Number` types.

  **Note**: It's really similar to `Invoicex.Enum.InvoiceType`, but different invoice tpyes could have same number type.
  Currently only `:pro_forma` invoice type is mapped to different (`:normal`) number type.
  """

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :number_type,
    values: [:corrective, :credit_notes, :estimated, :normal, :recurring]
end
