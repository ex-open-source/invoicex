defmodule Invoicex.Enum.EmailEvent do
  @moduledoc """
  Stores event types that could be optionally used in `Invoicex.Extra.Email` in regard to invoice event notification.
  """

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :email_event,
    values: [:correction, :delivery, :edition, :final_payment, :partial_payment]
end
