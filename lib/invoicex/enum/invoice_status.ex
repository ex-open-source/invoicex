defmodule Invoicex.Enum.InvoiceStatus do
  @moduledoc """
  Stores `Invoicex.Document.Invoice` status.
  """

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :invoice_status,
    values: [:draft, :proposed, :rejected, :approved, :partially_paid, :fully_paid, :archived]
end
