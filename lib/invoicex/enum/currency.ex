defmodule Invoicex.Enum.Currency do
  @moduledoc """
  This enum stores currencies provided by `ex_cldr` hex package.
  """

  currencies = Cldr.known_currencies()

  use EctoPostgresEnum,
    schema: Invoicex.schema_prefix(),
    type: :currency,
    values: Enum.sort(currencies)
end
