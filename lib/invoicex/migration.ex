defmodule Invoicex.Migration do
  @moduledoc """
  Module which runs `Invoicex`-specific migrations.
  """

  use Ecto.Migration

  alias Invoicex.Enum.{
    ContactNumberType,
    CountryCode,
    Currency,
    EmailEvent,
    InvoiceStatus,
    IntervalType,
    InvoiceType,
    NumberType
  }

  @enums [
    ContactNumberType,
    CountryCode,
    Currency,
    EmailEvent,
    InvoiceStatus,
    IntervalType,
    InvoiceType,
    NumberType
  ]
  @prefix Invoicex.schema_prefix()
  @tables ["invoices", "sources", "taxes", "discounts", "regions", "traders", "sets"]
  function = "invoicex_invoice_number_auto_increment"
  trigger = "invoicex_insert_invoice_number_trigger"
  full_prefix = if not is_nil(@prefix) and @prefix != "", do: @prefix <> ".", else: ""
  @function_name full_prefix <> function
  @invoices full_prefix <> "invoices"
  @trigger_name full_prefix <> trigger

  @doc """
  Drops all defined enums and tables.
  """
  @spec down :: :ok
  def down do
    Enum.each(@tables, &drop_if_exists(table(&1, prefix: @prefix)))
    Enum.each(@enums, & &1.drop_db_enum())
    execute("drop function if exists #{@function_name}()")
    execute("drop trigger if exists #{@trigger_name} on #{@invoices}")
  end

  @doc """
  Creates enums and tables.
  """
  @spec up :: :ok
  def up do
    Enum.each(@enums, & &1.create_db_enum())

    create_if_not_exists table("traders", prefix: @prefix) do
      add(:addresses, {:array, :map})
      add(:contacts, {:array, :map})
      add(:name, :string)
      add(:notes, {:array, :map})
      timestamps()
    end

    create_if_not_exists table("sets", prefix: @prefix) do
      add(:name, :string)
      add(:trader_id, references("traders"))
      timestamps()
    end

    create_if_not_exists table("regions", prefix: @prefix) do
      add(:name, :string)
      add(:set_id, references("sets"))
      timestamps()
    end

    create_if_not_exists table("discounts", prefix: @prefix) do
      add(:ends_at, :naive_datetime)
      add(:label, :string)
      add(:region_id, references("regions"))
      add(:starts_at, :naive_datetime)
      add(:value, :decimal)
      timestamps()
    end

    create_if_not_exists table("taxes", prefix: @prefix) do
      add(:description, :string)
      add(:is_compound, :boolean, default: false)
      add(:is_name_visible_on_invoice, :boolean, default: false)
      add(:is_recoverable, :boolean, default: false)
      add(:name, :string)
      add(:rate, :decimal)
      add(:region_id, references("regions"))
      add(:valid_from, :date)
      add(:valid_to, :date)
      timestamps()
    end

    create_if_not_exists table("sources", prefix: @prefix) do
      add(:delivery_set_id, references("sets"))
      add(:name, :string)
      add(:selling_region_id, references("regions"))
      add(:type, :string)
      add(:value, :string)
      timestamps()
    end

    create_if_not_exists table("invoices", prefix: @prefix) do
      add(:attachments, {:array, :map}, default: [])
      add(:buyer, :map)
      add(:buyer_trader_id, references("traders"))
      add(:correct_reasons, {:array, :map}, default: [])
      add(:currency, Currency.type())
      add(:custom_number, :string)
      add(:description, :string)
      add(:due_date, :date)
      add(:edit_reasons, {:array, :map}, default: [])
      add(:expected_payment_date, :date)
      add(:footer, :string)
      add(:groups, {:array, :map}, default: [])
      add(:issue_date, :date)
      add(:items, {:array, :map}, default: [])
      add(:notes, {:array, :map}, default: [])
      add(:number, :map)
      add(:pdf, :map)
      add(:seller, :map)
      add(:seller_trader_id, references("traders"))
      add(:status, InvoiceStatus.type())
      add(:title, :string)
      add(:type, InvoiceType.type())
      timestamps()
    end

    execute("""
    create or replace function #{@function_name}() returns trigger as $$
    begin
      select coalesce(new.number, '{}'::jsonb) || jsonb_build_object('value', coalesce(max(number->>'value')::integer + 1, 1))
        into new.number
        from #{@invoices}
        where number->>'type' = new.number->>'type';
      return new;
    end;
    $$ language plpgsql;
    """)

    execute("""
    create trigger #{@trigger_name} before insert on #{@invoices}
      for each row
      execute procedure #{@function_name}();
    """)

    execute("""
    create or replace function invoicex_append(items jsonb[], items2 jsonb, type varchar(8)) returns jsonb[] as $$
    declare
      item_array jsonb[];
      item jsonb;
    begin
      item_array = (select array_agg(data) from jsonb_array_elements(items2) data);
      foreach item in array(coalesce(item_array, '{}')) loop
        item = item || jsonb_build_object('__type__', type);
        items = array_append(items, item);
      end loop;

      return items;
    end;
    $$ language plpgsql;
    """)

    execute("""
    create or replace function invoicex_update_group_total(total decimal, item jsonb) returns decimal as $$
    begin
      case
        when (item->>'__type__') = 'discount' and (item->>'is_percent')::boolean = true then
          return total - total / 100 * (item->>'value')::decimal;
        when (item->>'__type__') = 'discount' then
          return total - (item->>'value')::decimal;
        when (item->>'__type__') = 'item' then
          return total + (item->>'price')::decimal * (item->>'quantity')::decimal;
        when (item->>'__type__') = 'tax' and (item->>'is_percent')::boolean = true then
          return total + total / 100 * (item->>'rate')::decimal;
        when (item->>'__type__') = 'tax' then
          return total + (item->>'rate')::decimal;
      end case;
    end;
    $$ language plpgsql;
    """)

    execute("""
    create or replace function invoicex_group_total(invoice_group jsonb) returns decimal as $$
    declare
      item jsonb;
      items jsonb[] = '{}';
      total decimal = 0;
    begin
      item = (invoice_group->>'item')::jsonb || jsonb_build_object('__type__', 'item', 'order', 0);
      items = array_append(items, item);
      items = invoicex_append(items, (invoice_group->>'discounts')::jsonb, 'discount');
      items = invoicex_append(items, (invoice_group->>'items')::jsonb, 'item');
      items = invoicex_append(items, (invoice_group->>'taxes')::jsonb, 'tax');

      for item in (select gi from unnest(items) gi order by gi->>'order') loop
        total = invoicex_update_group_total(total, item);
      end loop;

      return total;
    end;
    $$ language plpgsql;
    """)

    execute("""
    create or replace function invoicex_item_total(item jsonb) returns decimal as $$
    declare
      raw_total decimal;
      discounted_total decimal;
    begin
      raw_total = (item->>'price')::decimal * (item->>'quantity')::decimal;

      case
        when item->>'discount' is null then
          discounted_total = raw_total;
        when (item#>>'{discount,is_percent}')::boolean = true then
          discounted_total = raw_total - raw_total / 100 * (item#>>'{discount,value}')::decimal;
        else
          discounted_total = raw_total - (item#>>'{discount,value}')::decimal;
      end case;

      case
        when item->>'tax' is null then
          return discounted_total;
        when (item#>>'{tax,is_percent}')::boolean = true then
          return discounted_total + discounted_total / 100 * (item#>>'{tax,rate}')::decimal;
        else
          return discounted_total + (item#>>'{tax,rate}')::decimal;
      end case;
    end;
    $$ language plpgsql;
    """)

    execute("""
    create or replace function invoicex_total(invoice #{@invoices}) returns decimal as $$
    declare
      invoice_group jsonb;
      item jsonb;
      total decimal = 0;
    begin
      foreach invoice_group in array(invoice.groups) loop
        total = total + invoicex_group_total(invoice_group);
      end loop;

      foreach item in array(invoice.items) loop
        total = total + invoicex_item_total(item);
      end loop;

      return total;
    end;
    $$ language plpgsql;
    """)
  end
end
