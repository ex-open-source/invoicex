defmodule Invoicex.Extra.Email do
  @moduledoc """
  Helper email model.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Enum.EmailEvent

  @required_fields [:value]
  @fields @required_fields ++ [:accepts_html, :attachments_urls, :name, :on]

  embedded_schema do
    field(:accepts_html, :boolean, default: true)
    field(:attachments_urls, {:array, :string}, default: [])
    field(:name, :string, default: "")
    field(:on, EmailEvent)
    field(:value, :string)
  end

  @doc false
  def changeset(email \\ %__MODULE__{}, attrs \\ %{}) do
    email
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
