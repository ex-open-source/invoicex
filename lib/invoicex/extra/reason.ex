defmodule Invoicex.Extra.Reason do
  @moduledoc """
  Stores edit and correct reasons in `Invoicex.Document.Invoice`.
  """

  use Ecto.Schema

  alias Ecto.Changeset

  @fields [:at, :text]
  @required_fields @fields

  embedded_schema do
    field(:at, :naive_datetime)
    field(:text, :string)
  end

  @doc false
  def changeset(reason \\ %__MODULE__{}, attrs \\ %{}) do
    reason
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
