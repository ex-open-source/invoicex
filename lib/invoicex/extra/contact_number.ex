defmodule Invoicex.Extra.ContactNumber do
  @moduledoc """
  Helper model to store and validate contact numbers.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias ExPhoneNumber.Model.PhoneNumber
  alias Invoicex.Enum.ContactNumberType

  @required_fields [:country_code, :national_number]
  @fields [:type | @required_fields]

  embedded_schema do
    field(:country_code, :integer)
    field(:national_number, :integer)
    field(:type, ContactNumberType)
  end

  @doc false
  def changeset(phone_number \\ %__MODULE__{}, attrs \\ %{}) do
    phone_number
    |> Changeset.cast(attrs, @fields)
    |> custom_validate()
    |> Changeset.validate_required(@required_fields)
  end

  defp custom_validate(changeset) do
    country_code = Changeset.get_field(changeset, :country_code)
    national_number = Changeset.get_field(changeset, :national_number)
    data = %PhoneNumber{country_code: country_code, national_number: national_number}
    data_type = ExPhoneNumber.get_number_type(data)
    type = Changeset.get_field(changeset, :type)
    do_custom_validate(changeset, data, data_type, type)
  end

  defp do_custom_validate(changeset, data, data_type, type) do
    if ExPhoneNumber.is_valid_number?(data) do
      validate_type(changeset, data_type, type)
    else
      Changeset.add_error(changeset, :number, "invalid")
    end
  end

  defp validate_type(changeset, data_type, nil),
    do: Changeset.put_change(changeset, :type, data_type)

  defp validate_type(changeset, data_type, type) when data_type == type, do: changeset

  defp validate_type(changeset, _data_type, _type),
    do: Changeset.add_error(changeset, :type, "mismatch")
end
