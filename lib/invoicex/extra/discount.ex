defmodule Invoicex.Extra.Discount do
  @moduledoc """
  Extra embedded copy of `Invoicex.Ecommerce.Discount` which is used  by `Invoicex.Document.Group`.
  """

  use Ecto.Schema

  alias Ecto.Changeset

  @required_fields [:label, :value]
  @fields [:is_percent, :order] ++ @required_fields

  embedded_schema do
    field(:is_percent, :boolean, default: true)
    field(:label, :string)
    field(:order, :integer)
    field(:value, :decimal)
  end

  @doc false
  def changeset(discount \\ %__MODULE__{}, attrs \\ %{}, in_group \\ false) do
    discount
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
    |> Invoicex.validate_order(in_group)
  end
end
