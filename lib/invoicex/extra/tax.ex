defmodule Invoicex.Extra.Tax do
  @moduledoc """
  Extra embedded copy of `Invoicex.Ecommerce.Tax` which is used  by `Invoicex.Document.Group`.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.Tax, as: EcommerceTax
  alias Invoicex.Helper

  require Helper

  @required_fields [:name, :rate]
  @fields [:description, :is_compound, :is_percent, :is_recoverable, :order] ++ @required_fields

  embedded_schema do
    Helper.copy_fields_from(EcommerceTax, except: [:id, :rate, :region])
    field(:is_percent, :boolean)
    field(:order, :integer)
    field(:rate, :decimal)
  end

  @doc false
  def changeset(tax \\ %__MODULE__{}, attrs \\ %{}, in_group \\ false) do
    tax
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
    |> Invoicex.validate_order(in_group)
  end
end
