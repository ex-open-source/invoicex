defmodule Invoicex.Extra.Money do
  @moduledoc """
  Special model to store and validate moeny with currency or its percentage value.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Enum.Currency

  @required_fields [:currency, :value]
  @fields [:is_percent | @required_fields]

  embedded_schema do
    field(:currency, Currency)
    field(:is_percent, :boolean, default: false)
    field(:value, :decimal)
  end

  @doc false
  def changeset(money \\ %__MODULE__{}, attrs \\ %{}) do
    money
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
    |> custom_validate()
  end

  defp custom_validate(changeset) do
    currency = Changeset.get_field(changeset, :currency)
    is_percent = Changeset.get_field(changeset, :is_percent)
    do_custom_validate(changeset, currency, is_percent)
  end

  defp do_custom_validate(changeset, nil, true), do: changeset

  defp do_custom_validate(changeset, nil, false),
    do: Changeset.validate_required(changeset, [:currency])

  defp do_custom_validate(changeset, _currency, false), do: changeset

  defp do_custom_validate(changeset, currency, true) when not is_nil(currency),
    do: Changeset.add_error(changeset, :currency, "conflicts with is_percent which set to true")
end
