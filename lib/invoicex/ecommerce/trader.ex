defmodule Invoicex.Ecommerce.Trader do
  @moduledoc """
  A helper e-commerce model to group addresses and invoices.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Document.Invoice
  alias Invoicex.Ecommerce.{Contact, Address, Note, Set}

  @fields [:name]
  @required_fields @fields
  @schema_prefix Invoicex.schema_prefix()

  schema "traders" do
    embeds_many(:addresses, Address, on_replace: :delete)
    embeds_many(:contacts, Contact, on_replace: :delete)
    embeds_many(:notes, Note, on_replace: :delete)
    field(:name, :string)
    has_many(:bought_invoices, Invoice, foreign_key: :buyer_trader_id)
    has_many(:sets, Set)
    has_many(:sold_invoices, Invoice, foreign_key: :seller_trader_id)
    timestamps()
  end

  @doc false
  def changeset(address \\ %__MODULE__{}, attrs \\ %{}) do
    address
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:addresses)
    |> Changeset.cast_embed(:contacts)
    |> Changeset.cast_embed(:notes)
    |> Changeset.validate_required(@required_fields)
  end
end
