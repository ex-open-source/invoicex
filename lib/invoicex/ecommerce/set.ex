defmodule Invoicex.Ecommerce.Set do
  @moduledoc """
  Set is simple group of `Invoicex.Ecommerce.Region` which is defined to separate selling regions for specified `Invoicex.Ecommerce.Source` type.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.{Region, Source, Trader}

  @required_fields [:name]
  @fields @required_fields
  @schema_prefix Invoicex.schema_prefix()

  schema "sets" do
    belongs_to(:trader, Trader)
    field(:name, :string)
    has_many(:regions, Region)
    has_many(:sources, Source, foreign_key: :delivery_set_id)
    timestamps()
  end

  @doc false
  def changeset(set \\ %__MODULE__{}, attrs \\ %{}) do
    set
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
