defmodule Invoicex.Ecommerce.Note do
  @moduledoc """
  Optional data which could be add to `Invoicex.Document.Invoice`.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Document.Note, as: DocumentNote
  alias Invoicex.Helper

  require Helper

  @required_fields [:at, :label, :text, :valid_from, :valid_to]
  @fields @required_fields

  embedded_schema do
    Helper.copy_fields_from(DocumentNote, except: [:id])
    field(:valid_from, :date)
    field(:valid_to, :date)
  end

  @doc false
  def changeset(note \\ %__MODULE__{}, attrs \\ %{}) do
    note
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
