defmodule Invoicex.Ecommerce.Tax do
  @moduledoc """
  Covers lots of scenarios for working with various taxes.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.Region
  alias Invoicex.Extra.Money

  @required_fields [:name, :valid_from, :valid_to]
  @fields [:description, :is_compound, :is_recoverable, :region_id] ++ @required_fields
  @schema_prefix Invoicex.schema_prefix()

  schema "taxes" do
    belongs_to(:region, Region)
    embeds_one(:rate, Money, on_replace: :update)
    field(:description, :string)
    field(:is_compound, :boolean, default: false)
    field(:is_recoverable, :boolean, default: false)
    field(:name, :string)
    field(:valid_from, :date)
    field(:valid_to, :date)
    timestamps()
  end

  @doc false
  def changeset(tax \\ %__MODULE__{}, attrs \\ %{}) do
    tax
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:rate, required: true)
    |> Changeset.assoc_constraint(:region)
    |> Changeset.validate_required(@required_fields)
  end
end
