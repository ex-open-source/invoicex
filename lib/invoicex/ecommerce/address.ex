defmodule Invoicex.Ecommerce.Address do
  @moduledoc """
  An e-commerce version of `Invoicex.Document.Address`.

  The only difference are few extra fields which will help you in form for creating `Invoicex.Document.Buyer` and `Invoicex.Document.Seller` and `contacts` which allow to store contacts only for specified address.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Document.Address, as: DocumentAddress
  alias Invoicex.Ecommerce.Contact
  alias Invoicex.Helper

  require Helper

  @required_fields [:country_code, :data]
  @fields @required_fields

  embedded_schema do
    Helper.copy_fields_from(DocumentAddress, except: [:contact, :id])
    embeds_many(:contacts, Contact, on_replace: :delete)
    field(:is_default, :boolean, default: false)
    field(:is_default_billing, :boolean, default: false)
    field(:is_default_shipping, :boolean, default: false)
  end

  @doc false
  def changeset(address \\ %__MODULE__{}, attrs \\ %{}) do
    address
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:contacts)
    |> Changeset.validate_length(:data, min: 1)
    |> Changeset.validate_required(@required_fields)
  end
end
