defmodule Invoicex.Ecommerce.Region do
  @moduledoc """
  Regions keep relations to discounts and taxes for specified `Invoicex.Ecommerce.Set` of `Invoicex.Ecommerce.Source`.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.{Discount, Set, Source, Tax}

  @required_fields [:name]
  @fields [:set_id | @required_fields]
  @schema_prefix Invoicex.schema_prefix()

  schema "regions" do
    belongs_to(:set, Set)
    field(:name, :string)
    has_many(:discounts, Discount)
    has_many(:sources, Source, foreign_key: :selling_region_id)
    has_many(:taxes, Tax)
    timestamps()
  end

  @doc false
  def changeset(region \\ %__MODULE__{}, attrs \\ %{}) do
    region
    |> Changeset.cast(attrs, @fields)
    |> Changeset.assoc_constraint(:set)
    |> Changeset.validate_required(@required_fields)
  end
end
