defmodule Invoicex.Ecommerce.Contact do
  @moduledoc """
  An e-commerce version of `Invoicex.Document.Contact`.
  The only difference is one extra fields which is useful .
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Document.Contact, as: DocumentContact
  alias Invoicex.Helper

  require Helper

  @required_fields [:is_default, :names, :surname]
  @fields @required_fields ++ [:websites]

  embedded_schema do
    Helper.copy_fields_from(DocumentContact, except: [:contact, :id])
    field(:is_default, :boolean, default: false)
  end

  @doc false
  def changeset(contact \\ %__MODULE__{}, attrs \\ %{}) do
    contact
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:emails)
    |> Changeset.cast_embed(:numbers)
    |> Changeset.validate_length(:names, min: 1)
    |> Changeset.validate_required(@required_fields)
  end
end
