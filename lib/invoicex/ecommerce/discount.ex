defmodule Invoicex.Ecommerce.Discount do
  @moduledoc """
  Defines discounts for `Invoicex.Ecommerce.Source` which belongs to specified `Invoicex.Ecommerce.Region`.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.Region
  alias Invoicex.Extra.Money

  @required_fields [:ends_at, :label, :starts_at]
  @fields [:region_id] ++ @required_fields
  @schema_prefix Invoicex.schema_prefix()

  schema "discounts" do
    belongs_to(:region, Region)
    embeds_one(:value, Money, on_replace: :update)
    field(:ends_at, :naive_datetime)
    field(:label, :string)
    field(:starts_at, :naive_datetime)
    timestamps()
  end

  @doc false
  def changeset(discount \\ %__MODULE__{}, attrs \\ %{}) do
    discount
    |> Changeset.cast(attrs, @fields)
    |> Changeset.cast_embed(:value, required: true)
    |> Changeset.assoc_constraint(:region)
    |> Changeset.validate_required(@required_fields)
    |> validate_dates_order(:starts_at, :ends_at)
  end

  defp validate_dates_order(changeset, first_name, second_name) do
    if has_errors(changeset, [first_name, second_name]) do
      changeset
    else
      from = Changeset.get_field(changeset, first_name)
      to = Changeset.get_field(changeset, second_name)
      result = NaiveDateTime.compare(from, to)
      do_validate_dates_order(changeset, result, first_name, second_name)
    end
  end

  defp has_errors(changeset, fields), do: Enum.any?(fields, &has_error(changeset, &1))

  defp has_error(%{errors: errors}, field), do: Keyword.has_key?(errors, field)

  defp do_validate_dates_order(changeset, :lt, _first_name, _second_name), do: changeset

  defp do_validate_dates_order(changeset, :eq, first_name, second_name),
    do: Changeset.add_error(changeset, first_name, "cannot be equal to #{second_name}")

  defp do_validate_dates_order(changeset, :gt, first_name, second_name),
    do: Changeset.add_error(changeset, first_name, "cannot be greater than #{second_name}")
end
