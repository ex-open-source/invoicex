defmodule Invoicex.Ecommerce.Interval do
  @moduledoc """
  Defines `Invoicex.Document.Number` interval. It created to simplify working with recurring invoices.

  Interval covers lots of cases, for example with combination `type` and `steps` fields you can create a struct which will cover realy edge-case scenario like [4–4–5 calendar](https://en.wikipedia.org/wiki/4–4–5_calendar).
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Enum.IntervalType

  @required_fields [:only_business_days, :starts_on, :steps, :type]
  @fields @required_fields

  embedded_schema do
    field(:only_business_days, :boolean, default: false)
    field(:starts_on, :date)
    field(:steps, {:array, :integer}, default: [])
    field(:type, IntervalType)
  end

  @doc false
  def changeset(interval \\ %__MODULE__{}, attrs \\ %{}) do
    interval
    |> Changeset.cast(attrs, @fields)
    |> Changeset.validate_required(@required_fields)
  end
end
