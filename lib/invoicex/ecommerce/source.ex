defmodule Invoicex.Ecommerce.Source do
  @moduledoc """
  Defines source schema..
  Source basic information of invoice item source.
  For example `type` could be a database table name and `value` could be database row `id`.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias Invoicex.Ecommerce.{Region, Set}

  @required_fields [:name, :type, :value]
  @fields @required_fields ++ [:delivery_set_id, :selling_region_id]
  @schema_prefix Invoicex.schema_prefix()

  schema "sources" do
    belongs_to(:delivery_set, Set)
    belongs_to(:selling_region, Region)
    field(:name, :string)
    field(:type, :string)
    field(:value, :string)
    timestamps()
  end

  @doc false
  def changeset(source \\ %__MODULE__{}, attrs \\ %{}) do
    source
    |> Changeset.cast(attrs, @fields)
    |> Changeset.assoc_constraint(:delivery_set)
    |> Changeset.assoc_constraint(:selling_region)
    |> Changeset.validate_required(@required_fields)
  end
end
