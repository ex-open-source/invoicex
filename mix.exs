defmodule Invoicex.MixProject do
  use Mix.Project

  @version "1.0.0-rc.20"

  def application, do: [extra_applications: [:logger]]

  def project do
    [
      app: :invoicex,
      deps: [
        {:ecto_postgres_enum, "~> 1.0"},
        {:ex_cldr, "~> 1.5"},
        {:ex_cldr_territories, "~> 1.2"},
        {:ex_doc, "~> 0.19", only: :docs},
        {:ex_phone_number, "~> 0.1"},
        {:excoveralls, "~> 0.8", only: :test},
        {:jason, "~> 1.0"}
      ],
      description: "Ecto-based library to manage invoices",
      docs: [
        extras: ["README.md": [filename: "ecto_postgres_enum"]],
        main: "ecto_postgres_enum",
        source_ref: "v#{@version}",
        source_url: "https://gitlab.com/ex-open-source/ecto-postgres-enum"
      ],
      elixir: "~> 1.6",
      name: "Invoicex",
      package: [
        files: ~w(README.md lib mix.exs),
        licenses: ["MIT"],
        links: %{gitlab: "https://gitlab.com/ex-open-source/invoicex"},
        maintainers: ["Tomasz Sulima"]
      ],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        docs: :docs,
        "hex.publish": :docs
      ],
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      version: @version
    ]
  end
end
