# Invoicex

Ecto-based library to manage invoices

## Installation

Add `invoicex` to your deps:

```elixir
def deps do
  [
    {:invoicex, "~> 1.0.0-rc.0"}
  ]
end
```

Optionally set different ecto schema prefix for this library:

```
config :invoicex, schema_prefix: :invoices
```

Create your migration: `mix ecto.gen.migration invoicex` and add to it:

```
alias Invoicex.Migration

def up do
  Migration.up
end
```

See `Invoicex.Migration` for more information.

## More information

See [invoicex](https://hex.pm/packages/invoicex) hex package for more details.