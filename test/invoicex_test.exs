defmodule InvoicexTest do
  use ExUnit.Case

  alias Ecto.Changeset
  alias Invoicex.{Document, Extra}
  alias Invoicex.Document.{Buyer, Group, Invoice, Note}
  alias Invoicex.Ecommerce.Address, as: EcommerceAddress
  alias Invoicex.Ecommerce.Discount, as: EcommerceDiscount
  alias Invoicex.Ecommerce.{Region, Set, Source, Trader}
  alias Invoicex.Ecommerce.Tax, as: EcommerceTax
  alias Invoicex.Extra.{ContactNumber, Email, Reason}
  alias Invoicex.Helper

  @schemas [
    Buyer,
    ContactNumber,
    EcommerceAddress,
    EcommerceDiscount,
    EcommerceTax,
    Email,
    Invoice,
    Note,
    Reason,
    Region,
    Set,
    Source,
    Trader
  ]

  alias InvoicexTest.Fixture

  @aliases __ENV__.aliases |> Enum.map(fn {key, value} -> {value, key} end)

  test "changesets" do
    for schema <- @schemas do
      name = gen_function_name(schema)
      item = struct(schema)
      Fixture |> apply(:"#{name}_attrs", [:valid]) |> check_attrs(schema, item, :assert)
      Fixture |> apply(:"#{name}_attrs", [:invalid]) |> check_attrs(schema, item, :refute)
    end
  end

  defp check_attrs(attrs, schema, item, type) when is_list(attrs),
    do: Enum.map(attrs, &do_check_attrs(schema, item, &1, type))

  defp check_attrs(attrs, schema, item, type), do: do_check_attrs(schema, item, attrs, type)

  defp do_check_attrs(schema, item, attrs, type) do
    do_check_attrs(schema.changeset(item, attrs).valid?, type)
  end

  defp do_check_attrs(result, :assert), do: assert(result)
  defp do_check_attrs(result, :refute), do: refute(result)

  defp gen_function_name(schema),
    do: @aliases[schema] |> Module.split() |> List.last() |> Macro.underscore()

  test "build list" do
    invoice_attrs = Fixture |> apply(:invoice_attrs, [:valid]) |> List.first()
    changeset = Invoice.changeset(%Invoice{}, invoice_attrs)
    invoice = Changeset.apply_changes(changeset)
    assert Document.build_list(invoice) == Document.build_list(changeset)
  end

  test "build group list" do
    group_attrs = apply(Fixture, :group_attrs, [:valid])
    changeset = Group.changeset(%Group{}, group_attrs)
    group = Changeset.apply_changes(changeset)
    assert Document.build_group_list(group) == Document.build_group_list(changeset)
  end

  test "fields for" do
    assert Helper.fields_for(Reason) == [id: :binary_id, at: :naive_datetime, text: :string]
    assert Helper.fields_for(Reason, except: [:id]) == [at: :naive_datetime, text: :string]
    assert Helper.fields_for(Reason, only: [:text]) == [text: :string]
  end

  test "parse number" do
    contact_number = :valid |> Fixture.contact_number_attrs() |> List.first()
    assert {:ok, _} = parse(contact_number, :fixed_line)
    assert {:error, _} = parse(contact_number, nil)
    invalid_contact_number = Fixture.contact_number_attrs(:invalid)
    assert {:error, _} = parse(invalid_contact_number, :fixed_line)
  end

  test "total sql part" do
    assert Document.total_sql_part("") == Document.total_sql_part(nil)
    assert Document.total_sql_part("", nil) == Document.total_sql_part(nil, nil)
    assert Document.total_sql_part("test", :prefix) != Document.total_sql_part("test", :with)
  end

  defp parse(contact_number, type) do
    country_code = contact_number[:country_code]
    national_number = contact_number[:national_number]
    do_parse("+#{country_code} #{national_number}", "ge", type)
  end

  defp do_parse(number, country_iso, nil), do: Extra.parse_number(number, country_iso)
  defp do_parse(number, country_iso, type), do: Extra.parse_number(number, country_iso, type)

  test "schema prefix" do
    assert Invoicex.schema_prefix() == nil
    Application.put_env(:invoicex, :schema_prefix, :public)
    assert Invoicex.schema_prefix() == :public
  end

  test "valid currency" do
    assert Extra.valid_currency?(:PL, :PLN)
    assert Extra.valid_currency?(:PL, :PLZ, ~D[1975-01-01])
    assert Extra.valid_currency?(:US, :USS, ~D[2014-01-01], false)
  end
end
