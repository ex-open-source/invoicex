defmodule InvoicexTest.Fixture do
  @now NaiveDateTime.utc_now()
  @one_second_later NaiveDateTime.add(@now, 1)

  def address_attrs(:invalid), do: %{}

  def address_attrs(:valid),
    do: %{
      contact: contact_attrs(:valid),
      country_code: :US,
      data: ["132, My Street,", "Kingston, New York 12401"]
    }

  def buyer_attrs(:invalid), do: %{}

  def buyer_attrs(:valid) do
    address = address_attrs(:valid)

    attrs = %{
      billing_address: address,
      contact: contact_attrs(:valid),
      customer: "Example",
      shipping_address: address
    }

    attrs2 = attrs |> Map.delete(:shipping_address) |> Map.put_new(:copy_billing_address, true)
    [attrs, attrs2]
  end

  def contact_attrs(:invalid), do: %{}

  def contact_attrs(:valid),
    do: %{names: ["John", "Alfred"], numbers: contact_number_attrs(:valid), surname: "Smith"}

  def contact_number_attrs(:invalid),
    do: [%{}, %{country_code: 49, national_number: 8_963_648_018, type: :mobile}]

  def contact_number_attrs(:valid) do
    base = %{country_code: 49, national_number: 8_963_648_018}
    [base, Map.put_new(base, :type, :fixed_line)]
  end

  def discount_attrs(:invalid), do: %{}

  def discount_attrs(:valid) do
    %{
      ends_at: @one_second_later,
      label: "Example",
      order: 1,
      starts_at: @now,
      value: Map.fetch!(money_attrs(), :value)
    }
  end

  def ecommerce_address_attrs(:invalid), do: %{}

  def ecommerce_address_attrs(:valid) do
    {contact_attrs, attrs} = Map.pop(address_attrs(:valid), :contact)
    Map.put_new(attrs, :contacts, [contact_attrs])
  end

  def ecommerce_discount_attrs(:invalid) do
    value = money_attrs()

    [
      %{},
      %{ends_at: @now, starts_at: @now},
      %{ends_at: @now, starts_at: @one_second_later},
      %{value: Map.put_new(value, :is_percent, true)},
      %{value: Map.put(value, :currency, nil)},
      %{value: Map.merge(value, %{currency: nil, is_percent: true})}
    ]
  end

  def ecommerce_discount_attrs(:valid),
    do: :valid |> discount_attrs() |> Map.replace!(:value, money_attrs())

  def ecommerce_note_attrs(:valid),
    do: :valid |> note_attrs() |> Map.merge(%{valid_from: @now, valid_to: @one_second_later})

  def ecommerce_tax_attrs(:invalid), do: %{}

  def ecommerce_tax_attrs(:valid) do
    :valid
    |> tax_attrs()
    |> Map.merge(%{rate: money_attrs(), valid_from: @now, valid_to: @one_second_later})
  end

  def email_attrs(:invalid), do: %{}
  def email_attrs(:valid), do: %{on: :delivery, value: "sample@example.com"}

  def group_attrs(:invalid), do: %{}

  def group_attrs(:valid) do
    item_attrs = item_attrs(:valid)

    %{
      discounts: [discount_attrs(:valid)],
      item: item_attrs,
      items: [Map.put_new(item_attrs, :order, 2)],
      order: 2,
      taxes: [tax_attrs(:valid)]
    }
  end

  def interval_attrs(:invalid), do: %{}
  def interval_attrs(:valid), do: %{starts_on: @now, type: :monthly}

  def invoice_attrs(:invalid) do
    valid_item_with_invalid_order = :valid |> item_attrs() |> Map.put_new(:order, 2)
    [%{}, %{items: [valid_item_with_invalid_order]}]
  end

  def invoice_attrs(:valid) do
    buyer_attrs = :valid |> buyer_attrs() |> List.last()
    item_attrs = :valid |> item_attrs() |> Map.put_new(:order, 1)

    attrs = %{
      attachments: [%{url: "/tmp/image.png"}],
      buyer: buyer_attrs,
      buyer_trader_id: 2,
      currency: :USD,
      groups: [group_attrs(:valid)],
      issue_date: @now,
      items: [item_attrs],
      number: number_attrs(:valid),
      seller: seller_attrs(:valid),
      seller_trader_id: 1,
      total: money_attrs(),
      title: "Example invoice",
      type: :normal
    }

    [attrs, Map.put(attrs, :type, :pro_forma)]
  end

  def item_attrs(:invalid), do: %{}

  def item_attrs(:valid) do
    money_attrs = Map.fetch!(money_attrs(), :value)
    %{name: "Example product/service", price: money_attrs, quantity: 1}
  end

  def money_attrs, do: %{currency: :USD, value: 100}

  def note_attrs(:invalid), do: %{}

  def note_attrs(:valid),
    do: %{at: @now, label: "Something happened", text: "You need to pay a bit more …"}

  def number_attrs(:invalid), do: %{}
  def number_attrs(:valid), do: %{interval: interval_attrs(:valid), type: :normal, value: 1}

  def reason_attrs(:invalid), do: %{}
  def reason_attrs(:valid), do: %{at: @now, text: "Fixed mistake caused by noob"}

  def region_attrs(:invalid), do: %{}
  def region_attrs(:valid), do: %{name: "New York"}

  def seller_attrs(:invalid), do: %{}

  def seller_attrs(:valid),
    do: %{
      address: address_attrs(:valid),
      contact: contact_attrs(:valid),
      logo_url: "/tmp/logo.png",
      vendor: "Example"
    }

  def set_attrs(:invalid), do: %{}
  def set_attrs(:valid), do: %{name: "A special type of our awesome products …"}

  def source_attrs(:invalid), do: %{}

  def source_attrs(:valid),
    do: %{
      name: "Product name …",
      type: "MyJavaApp.Product",
      value: "7ecc3183-41d0-4548-b3fa-4fa5871e6d89"
    }

  def tax_attrs(:invalid), do: %{}
  def tax_attrs(:valid), do: %{name: "Example", order: 3, rate: 1}

  def trader_attrs(:invalid), do: %{}
  def trader_attrs(:valid), do: %{name: "John Smith", notes: [ecommerce_note_attrs(:valid)]}
end
